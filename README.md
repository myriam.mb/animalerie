
# Installation :

- npm install
- AJouter plugin cors :
https://addons.mozilla.org/fr/firefox/addon/cors-everywhere/

# gitignore
- node_modules
- dist
- cache

# Contexte du projet

Afin de gérer son animalerie, M. Dolittle utilise un fichier JSON. En revanche, il a besoin de votre aide pour récupérer et afficher les informations que contient celui-ci.

Commencez par récupérer le fichier users.json proposé dans ce brief.

Initialisez un projet avec les fichiers suivants :

- index.html
- app.js (à appeler dans index.html)
- users.json

- Grâce à l'API Fetch récupérez et affichez le contenu du fichier JSON dans la console (utilisateurs et animaux).
- N'affichez que la liste des utilisateurs.
- N'affichez que la liste des animaux, classés dans l'ordre alphabétique de leurs noms.
- N'affichez dans la console que les utilisateurs qui possèdent au moins un animal.
- Ajoutez un animal Mickey, de type souris, âgé de 0.1 an à chaque utilisateur puis affichez la liste des utilisateurs.
- En bonus, parce que le propriétaire adore les chats : affichez 5 faits au hasard concernant les chats, en vous appuyant sur- l'API Cat Facts : https://alexwohlbruck.github.io/cat-facts/docs/

# Ressources :

- Users json : https://simplonline-v3-prod.s3.eu-west-3.amazonaws.com/media/file/txt/1112323f-2d3c-435b-82e2-3d0354b59ee1.txt

# Livrables

Un repo gitlab et une version en ligne.
